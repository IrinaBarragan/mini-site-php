<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Lac Baïkal</title>
</head>
<body>

<article>
	<h1>Lac Baïkal</h1>
	<p>Le lac Baïkal (en russe : Озеро Байкал, Ozero Baïkal) est un lac situé dans le sud de la Sibérie, en Russie orientale. Il constitue la plus grande réserve d'eau douce liquide à la surface de la Terre. La transparence de ses eaux permet une visibilité parfaite jusqu'à 40 mètres de profondeur. Il est parfois surnommé la « Perle de Sibérie ». Pour ses premiers habitants, les différents peuples turcs et mongols, le lac était une mer sacrée, Baïkal provenant du turc Bay Köl (« lac riche/sacré »)</p>
</article>

<div class="grid">
	<div class = "item">
		<img src='images/1.jpg' alt ="1">
	</div>
	<div class = "item">
		<img src='images/2.jpg' alt ="2">
	</div>
	<div class = "item">
		<img src='images/3.jpg' alt ="3"">
	</div>
	<div class = "item">
		<img src='images/4.webp' alt ="4">
	</div>
		<div class = "item">
		<img src='images/5.jpg' alt ="5">
	</div>
		<div class = "item">
		<img src='images/6.jpg' alt ="6">
	</div>

	

	
</div>



</body>
</html>




